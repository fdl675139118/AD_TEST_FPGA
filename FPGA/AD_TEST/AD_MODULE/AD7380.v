
//AD7380

module ad7380(
    clk    ,
    rst_n  ,
    //�����ź�,����dout
    cs,
    sclk,
    sdi,
    sdoa,
    sdob,
    rxdata,
    rxdata_vld
    );

    //��������
    //parameter      DATA_W =         8;

    //�����źŶ���
    input               clk    ;
    input               rst_n  ;
    input sdoa;
    input sdob;

    //�����źŶ���
    //output[DATA_W-1:0]  dout   ;
    output cs;
    output sclk;
    output sdi;
    output rxdata;
    output rxdata_vld;
    //�����ź�reg����
   //reg   [DATA_W-1:0]  dout   ;

    //�м��źŶ���
    //reg                 signal1;

   
    reg [15:0] sclk_jsq0;
    reg [15:0] sclk_jsq1;
    wire add_sclk_jsq0,end_sclk_jsq0;
    wire add_sclk_jsq1,end_sclk_jsq1;
    
    reg bz_sclk;

    //duxie jicunqi
    always @(posedge clk or negedge rst_n)begin
        if(!rst_n)begin
            sclk_jsq0 <= 0;
        end
        else if(add_sclk_jsq0)begin
            if(end_sclk_jsq0)
                sclk_jsq0 <= 0;
            else
                sclk_jsq0 <= sclk_jsq0 + 1;
        end
    end

    assign add_sclk_jsq0 = 1;
    assign end_sclk_jsq0 = add_sclk_jsq0 && sclk_jsq0==2-1;   //80/2 M

    always @(posedge clk or negedge rst_n)begin 
        if(!rst_n)begin
            sclk_jsq1 <= 0;
        end
        else if(add_sclk_jsq1)begin
            if(end_sclk_jsq1)
                sclk_jsq1 <= 0;
            else
                sclk_jsq1 <= sclk_jsq1 + 1;
        end
    end

    assign add_sclk_jsq1 = end_sclk_jsq0;
    assign end_sclk_jsq1 = add_sclk_jsq1 && sclk_jsq1==20-1;   //16B+2B

    reg [15:0] jsq_cmd0;
    //reg [15:0] jsq_cmd1;
    wire add_jsq_cmd0,end_jsq_cmd0;
   // wire add_jsq_cmd1,end_jsq_cmd1;

    always @(posedge clk or negedge rst_n)begin
        if(!rst_n)begin
            jsq_cmd0 <= 0;
        end
        else if(add_jsq_cmd0)begin
            if(end_jsq_cmd0)
                jsq_cmd0 <= 0;
            else
                jsq_cmd0 <= jsq_cmd0 + 1;
        end
    end

    assign add_jsq_cmd0 = end_sclk_jsq1;
    assign end_jsq_cmd0 = add_jsq_cmd0 && jsq_cmd0==8-1;

    /*
    always @(posedge clk or negedge rst_n)begin 
        if(!rst_n)begin
            jsq_cmd1 <= 0;
        end
        else if(add_jsq_cmd1)begin
            if(end_jsq_cmd1)
                jsq_cmd1 <= 0;
            else
                jsq_cmd1 <= jsq_cmd1 + 1;
        end
    end

    assign add_jsq_cmd1 = end_jsq_cmd0;
    assign end_jsq_cmd1 = add_jsq_cmd1 && jsq_cmd1==5-1;
    */

    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            bz_sclk <= 1;
        end /*
        else if()begin
            bz_sclk <= 1;
        end */
        else if(end_jsq_cmd0)begin
            bz_sclk <= 0;
        end
    end
    

    wire sclk_fedge,sclk_redge;
    assign sclk_fedge = add_sclk_jsq0&&sclk_jsq0==1-1 && sclk_jsq1>1-1&&sclk_jsq1<18-1;
    assign sclk_redge = add_sclk_jsq0&&sclk_jsq0==2-1 && sclk_jsq1>1-1&&sclk_jsq1<18-1;
    reg sclk;

    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            sclk <= 1;
        end
        else if(sclk_fedge)begin
            sclk <= 0;
        end
        else if(sclk_redge)begin
            sclk <= 1;
        end
    end
    

    wire cs_fedge,cs_redge;
    assign cs_fedge = add_sclk_jsq0&&sclk_jsq0==1-1 && sclk_jsq1==1-1;
    assign cs_redge = add_sclk_jsq0&&sclk_jsq0==1-1 && sclk_jsq1==18-1;
    reg cs;
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            cs <= 1;
        end
        else if(cs_fedge)begin
            cs <= 0;
        end
        else if(cs_redge)begin
            cs <= 1;
        end
    end
    
    reg [15:0] txdata;
    reg sdi;

    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            sdi <= 0;
        end
        else if(add_sclk_jsq0&&sclk_jsq0==2-1 && sclk_jsq1<=16-1&& bz_sclk)begin   //sclk rise edge
            sdi <= txdata[15-sclk_jsq1];
        end        
    end

    reg [31:0] rxdata;
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            rxdata <= 0;
        end
        else if(sclk_redge)begin
            rxdata[16-sclk_jsq1] <= sdoa;
            rxdata[32-sclk_jsq1] <= sdob;
        end       
    end 

    reg rxdata_vld;
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            rxdata_vld <= 0;
        end
        else if(cs_redge)begin
            rxdata_vld <= 1;
        end
        else begin
            rxdata_vld <= 0;
        end
    end



     parameter      CMM1 =         16'b1001_0000_0000_0000;
     parameter      CMM2 =         16'b1010_0000_1111_1111;
     parameter      CMM3 =         16'b1011_0000_0000_0000;
     parameter      CMM4 =         16'b1100_0000_0000_0000;
     parameter      CMM5 =         16'b1101_1111_1111_1111;
     parameter      CMR1 =         16'b0001_0000_0000_0000;
     parameter      CMR2 =         16'b0010_0000_0000_0000;
     parameter      CMR3 =         16'b0011_0000_0000_0000;
     parameter      CMR4 =         16'b0100_0000_0000_0000;
     parameter      CMR5 =         16'b0101_0000_0000_0000;
     parameter      NOP  =         16'h0000;

    //wire [4*16-1:0] command_w;
    //wire [5*16-1:0] command_r;
    wire [8*16-1:0] command;
    //assign command_w = {NOP,CMM2,CMM1,NOP};
    //assign command_r = {NOP,NOP,CMR2,CMR1,NOP};
    assign command   = {NOP,NOP,CMR2,CMR3,NOP,NOP,CMM2,NOP}; 
    
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            txdata <= 0;
        end
        else if(cs_fedge&& bz_sclk)begin
            txdata <= command[(jsq_cmd0+1)*16-1-:16];
        end        
    end 

    endmodule








