
//ADS8588 AD閲囬泦妯″潡

//妯″潡鏃堕挓锛0M

module ads8588_cal(
    clk    ,
    rst_n  ,
   
    ad_rst_en,
    ad_busy,
    ad_frstdata,
    ad_rst,
    convsta,
    convstb,    
    ad_cs,
    ad_rd,
    ad_data,
    dout_vld,
    dout   
    );

    //锟斤拷锟斤拷锟斤拷锟斤拷
    //parameter      DATA_W =         8;

    //锟斤拷锟斤拷锟脚号讹拷锟斤拷
    input               clk    ;
    input               rst_n  ;
    
    input               ad_rst_en;
    input               ad_busy;
    input               ad_frstdata;
    input[15:0]         ad_data;

    //锟斤拷锟斤拷锟脚号讹拷锟斤拷
    output              ad_rst   ;
    output              convsta  ;
    output              convstb  ;
    output              ad_rd    ;
    output              ad_cs    ;
    output              dout_vld ;
    output[23:0]        dout     ;
     
    //锟斤拷锟斤拷锟脚猴拷reg锟斤拷锟斤拷
    reg                 ad_rst    ;
    reg                 convsta   ;
    reg                 convstb   ;
    reg                 ad_rd     ;
    reg                 ad_cs = 1 ;
    reg                 dout_vld  ;
    reg [23:0]          dout  = 24'h00_00_00;

    //锟叫硷拷锟脚号讹拷锟斤拷  
    reg                       flag_rst = 1; //flag_rst锟节诧拷使锟斤拷锟斤拷效
    reg                       flag_conv;
    reg                       flag0;
    reg [31:0]                adrst_jsq;
    reg [15:0]                conv_jsq;
    reg [15:0]                rd_jsq0;
    reg [15:0]                rd_jsq1;
    reg [15:0]                rd_jsq2;
    wire                      add_adrst_jsq;
    wire                      end_adrst_jsq;
    wire                      add_conv_jsq;
    wire                      end_conv_jsq;
    wire                      add_rd_jsq0;
    wire                      end_rd_jsq0;
    wire                      add_rd_jsq1;
    wire                      end_rd_jsq1;
    wire                      add_rd_jsq2;
    wire                      end_rd_jsq2;

    wire                      rd_mid;
    //reg                       ad_busy_ff0;
    //reg                       ad_busy_ff1;
    //reg                       ad_busy_ff2;   
   
	/*
	* RST 
    */    
    always @(posedge clk or negedge rst_n)begin
        if(!rst_n)begin
            adrst_jsq <= 0;
        end
        else if(add_adrst_jsq)begin
            if(end_adrst_jsq)
                adrst_jsq <= 0;          
            
            else
                adrst_jsq <= adrst_jsq + 1;
        end
    end

    assign add_adrst_jsq = flag_rst;       
    assign end_adrst_jsq = add_adrst_jsq && adrst_jsq== 5000-1;  //锟斤拷位之锟襟，等达拷一锟斤拷时锟斤拷  

    //flag_rst
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin    //系统锟斤拷位锟斤拷锟斤拷ad_rst也锟斤拷锟叫革拷位
            flag_rst <= 1;             
        end
        else if(ad_rst_en) begin    //锟解部使锟斤拷锟斤拷效时
            flag_rst <= 1;           
        end
        else if(end_adrst_jsq) begin
            flag_rst <= 0;
        end
    end        

    //ad_rst 	楂樼數骞冲浣嶏紙50ns浠ヤ笂锛    
	 always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            ad_rst <= 0;
        end
        else if(add_adrst_jsq && adrst_jsq== 1-1)begin //锟竭碉拷平锟斤拷锟斤拷小50ns
            ad_rst <= 1;
        end
        else if(add_adrst_jsq && adrst_jsq== 100-1)begin  //锟竭碉拷平锟斤拷锟斤拷小50ns
            ad_rst <= 0;
        end
    end
    
    /*
    * AD_CAL    //Tcyc(min) = 2000ns,conv_jsq =100 
    */
	 
   always @(posedge clk or negedge rst_n)begin
       if(!rst_n)begin
           conv_jsq <= 0;
       end
       else if(add_conv_jsq)begin
           if(end_conv_jsq)
               conv_jsq <= 0;
           else
               conv_jsq <= conv_jsq + 1;
       end
   end

   assign add_conv_jsq = flag_conv;       
   assign end_conv_jsq = add_conv_jsq && conv_jsq==65-1 ;  //Tconv = 1290ns,high power period    

   always  @(posedge clk or negedge rst_n)begin
       if(rst_n==1'b0)begin
           flag_conv <= 0;
       end
       else if(end_adrst_jsq || end_rd_jsq2)begin
           flag_conv <= 1;
       end
       else if(end_conv_jsq)begin
           flag_conv <= 0;
       end
   end  

   //convsta,convstb
   always  @(posedge clk or negedge rst_n)begin
       if(rst_n==1'b0)begin
           convsta <= 1;
           convstb <= 1;
       end
       else if(add_conv_jsq && conv_jsq==1-1)begin
           convsta <= 0;
           convstb <= 0;
       end
       else if(add_conv_jsq && conv_jsq==10-1)begin    //锟酵碉拷平min = 25ns锟斤拷2*conv_jsq = 40ns
           convsta <= 1;
           convstb <= 1;
       end
   end

	/*
    * rd data
    */ 

/*
  //ad_busy_ffx
  always  @(posedge clk or negedge rst_n)begin
      if(rst_n==1'b0)begin
          ad_busy_ff0 <= 0;
          ad_busy_ff1 <= 0;
          ad_busy_ff2 <= 0;
      end
      else begin
          ad_busy_ff0 <= ad_busy;
          ad_busy_ff1 <= ad_busy_ff0;
          ad_busy_ff2 <= ad_busy_ff1;
      end
     
  end
*/

/*绛夊緟杞崲缁撴潫锛孯Dn浣胯兘*/
//ad_rd

/*ad_read*/
always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        rd_jsq0 <= 0;
    end
    else if(add_rd_jsq0)begin
        if(end_rd_jsq0)
            rd_jsq0 <= 0;
        else
            rd_jsq0 <= rd_jsq0 + 1;
    end
end

assign add_rd_jsq0 = flag0;
assign end_rd_jsq0 = add_rd_jsq0 && rd_jsq0== 4-1;  //ad_read_per:30ns

always @(posedge clk or negedge rst_n)begin 
    if(!rst_n)begin
        rd_jsq1 <= 0;
    end
    else if(add_rd_jsq1)begin
        if(end_rd_jsq1)
            rd_jsq1 <= 0;
        else
            rd_jsq1 <= rd_jsq1 + 1;
    end
end

assign add_rd_jsq1 = end_rd_jsq0;
assign end_rd_jsq1 = add_rd_jsq1 && rd_jsq1== 2-1;   //2

always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        rd_jsq2 <= 0;
    end
    else if(add_rd_jsq2)begin
        if(end_rd_jsq2)
            rd_jsq2 <= 0;
        else
            rd_jsq2 <= rd_jsq2 + 1;
    end
end

assign add_rd_jsq2 = end_rd_jsq1;       
assign end_rd_jsq2 = add_rd_jsq2 && rd_jsq2== 8-1; //8channel

//flag0
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        flag0 <= 0;
    end
    else if(end_conv_jsq)begin
        flag0 <= 1;
    end
    else if(end_rd_jsq2)begin
        flag0 <= 0;
    end
end

assign rd_mid = add_rd_jsq0&&rd_jsq0==2;	//4/2

//ad_rd
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        ad_rd <= 1;
    end
    else if(add_rd_jsq0&&rd_jsq0==0)begin
        ad_rd <= 0;
    end
    else if(rd_mid)begin
        ad_rd <= 1;
    end
end

//ad_cs
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        ad_cs <= 1;
    end
    else if(end_conv_jsq)begin
        ad_cs <= 0;
    end
    else if(end_rd_jsq2)begin
        ad_cs <= 1;
    end
end

//dout = ad_data[17:0]
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        dout <= 24'h00_00_00;
    end
    else if(rd_mid && rd_jsq1==1-1)begin
        dout[17:2] <= ad_data[15:0 ];
    end
    else if(rd_mid && rd_jsq1==2-1)begin
        dout[1 :0] <= ad_data[15:14];
    end
end

//dout_vld
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        dout_vld <= 0;
    end
    else begin
        dout_vld <= rd_mid && rd_jsq1==2-1;
    end
   
end





    


    endmodule


