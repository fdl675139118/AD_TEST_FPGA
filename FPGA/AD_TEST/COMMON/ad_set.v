//ʱ��32M


module ad_set(
    clk         ,
    rst_n       ,
    //�����ź�,����dout
    dep_flag    ,
    ad_wr_en    ,
	 intf_dep_en ,
	 intf_dep_data,
	 
	 MesageHeader,
	 msgH_vld,
	 MesageEnder,
	 msgE_vld
        
    );

    //��������
    parameter      FIRE_TIME_MS = 2_000;        //2000ms
    parameter      DELAY_TIME   = 1000;        //20us : 20*50 = 1000
	 parameter      CH			  = 1;
	 parameter		 SIZE         = 4;
	 parameter		 DEPTH		  = 256;  
    
    reg [15:0]     sdepth;      
	 
    //�����źŶ���
    input               clk    ;
    input               rst_n  ;
	 
    input               dep_flag    ;
    input					intf_dep_en ;
	 input[15:0]			intf_dep_data;

    //�����źŶ���
   //output[DATA_W-1:0]  dout   ;
    output              ad_wr_en    ;
	 output        		MesageHeader;
	 output					msgH_vld;
	 output        		MesageEnder;
	 output					msgE_vld;
    //�����ź�reg����
    //reg   [DATA_W-1:0]  dout   ;   
    reg				ad_wr_en = 0;
    //�м��źŶ���    
    wire            add_fire_jsq,       end_fire_jsq;
	 wire            add_fire_jsq2,       end_fire_jsq2;
    reg [15:0]      fire_jsq; 
	 reg [15:0]      fire_jsq2; 	 
    reg             fire_in_en;
    wire            add_delay_jsq,      end_delay_jsq;
    reg [15:0]      delay_jsq; 
    reg             delay_flag;
    wire		        add_dep_jsq,		end_dep_jsq;
    reg[15:0]		  dep_jsq;
    reg             ad_en = 0;  
    reg             dep_flag_en;
    
	 //消息头类型
	 wire[15:0] frame_hd = 16'h5AA5;
	 wire[15:0] mlenth;
	 wire[15:0] mtype = 0;  
	 wire[47:0] MesageHeader;
	 reg msgH_vld;
	 
	 assign mlenth = sdepth*CH*SIZE + 4;	//24B
	 assign MesageHeader = {mtype,mlenth,frame_hd};
	 //assign msgH_vld = fire_in_en;
	 wire[15:0] MesageEnder = 16'hAAAA;
	 reg msgE_vld;
	 
	 //采集深度赋值
	 reg [15:0] temp_dep_data;
	 always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
				temp_dep_data <= DEPTH;	//默认设置256个采集深度
        end
        else if(intf_dep_en) begin
            temp_dep_data <= intf_dep_data;
        end  
    end
	 
	 always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
				sdepth <= 256;	//默认设置256个采集深度
        end
        else if(fire_in_en) begin
            sdepth <= temp_dep_data;
        end  
    end
	 
	always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
				msgH_vld <= 0;	
        end
        else begin
            msgH_vld <= fire_in_en;
        end  
    end

	always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
				msgE_vld <= 0;	
        end
        else if(add_fire_jsq2 && fire_jsq2== FIRE_TIME_MS-50) begin
            msgE_vld <= 1;
        end 
		  else
				msgE_vld <= 0;
    end	 	 
	 
    //////////////////////////////////////////////////////////////
    //
    /***fire_jsq***/
    always @(posedge clk or negedge rst_n)begin
        if(!rst_n)begin
            fire_jsq <= 0;
        end
        else if(add_fire_jsq)begin
            if(end_fire_jsq)
                fire_jsq <= 0;
            else
                fire_jsq <= fire_jsq + 1;
        end
    end

    assign add_fire_jsq = 1;       
    assign end_fire_jsq = add_fire_jsq && fire_jsq== 50000-1; //1ms
	 
	/***fire_jsq2***/
    always @(posedge clk or negedge rst_n)begin
        if(!rst_n)begin
            fire_jsq2 <= 0;
        end
        else if(add_fire_jsq2)begin
            if(end_fire_jsq2)
                fire_jsq2 <= 0;
            else
                fire_jsq2 <= fire_jsq2 + 1;
        end
    end

    assign add_fire_jsq2 = end_fire_jsq;       
    assign end_fire_jsq2 = add_fire_jsq2 && fire_jsq2== FIRE_TIME_MS-1; //2000ms 
    
    //dep_flag_en
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            dep_flag_en <= 0;
        end
        else if(dep_flag) begin
            dep_flag_en <= 1;
        end  
    end


    //fire_in_en
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            fire_in_en <= 0;
        end
        else if(add_fire_jsq && fire_jsq==1-1 && fire_jsq2==1-1 && dep_flag_en)begin
				fire_in_en <= 1;
        end
        else begin
            fire_in_en <= 0;
        end
    end
   
    /***delay_jsq***/
    always @(posedge clk or negedge rst_n)begin
        if(!rst_n)begin
            delay_jsq <= 0;
        end
        else if(add_delay_jsq)begin
            if(end_delay_jsq)
                delay_jsq <= 0;
            else
                delay_jsq <= delay_jsq + 1;
        end
    end

    assign add_delay_jsq = delay_flag;       
    assign end_delay_jsq = add_delay_jsq && delay_jsq== DELAY_TIME-1; 
   
    //delay_flag
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            delay_flag <= 0;
        end
        else if(fire_in_en)begin
            delay_flag <= 1;
        end
        else if(end_delay_jsq)begin
            delay_flag <= 0;
        end
    end
   
    //ad_en
   always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            ad_en <= 0;
        end
        else if(end_delay_jsq)begin
            ad_en <= 1;
        end
        else if(end_dep_jsq) begin
            ad_en <= 0;
        end
    end
  
    /***dep_jsq***/
    always @(posedge clk or negedge rst_n)begin
        if(!rst_n)begin
            dep_jsq <= 0;
        end
        else if(add_dep_jsq)begin
            if(end_dep_jsq)
                dep_jsq <= 0;
            else
                dep_jsq <= dep_jsq + 1;
        end
    end

    assign add_dep_jsq = dep_flag && ad_en;       
    assign end_dep_jsq = add_dep_jsq && dep_jsq== sdepth;	//采集深度计数
   
    //ad_wr_en
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            ad_wr_en <= 0;
        end
        else if(add_dep_jsq && dep_jsq==0)begin
            ad_wr_en <= 1;
        end
        else if(end_dep_jsq) begin
            ad_wr_en <= 0;
        end
    end
	 
	 

	 
	 
    endmodule

