module data_m2o(
    clk    ,
    rst_n  ,
    //�����ź�,����dout
    indat1,
    indat1_vld,
    indat2,
    indat2_vld,
	 indat3,
    indat3_vld,
    outdat,
    outdat_vld,
	 
    );

    //��������
    parameter      DATA_W =         8;

    //�����źŶ���
    input               clk    ;
    input               rst_n  ;
    
    input[DATA_W-1:0]   indat1;
    input               indat1_vld;
    input[DATA_W-1:0]   indat2;
    input               indat2_vld;
	 input[DATA_W-1:0]   indat3;
    input               indat3_vld;

    //�����źŶ���
    output              outdat;
    output              outdat_vld;
   


    //�����ź�reg����
    reg   [DATA_W-1:0]  outdat   ;
    wire                 outdat_vld;
    //�м��źŶ���
    //reg                 signal1;

   
    always  @(*)begin
        if(indat1_vld)begin
             outdat = indat1;
        end
        else if(indat2_vld) begin
             outdat = indat2;
        end
		  else if(indat3_vld) begin
             outdat = indat3;
        end
        else
             outdat = 0;
    end

assign outdat_vld = indat1_vld || indat2_vld|| indat3_vld;






    endmodule

