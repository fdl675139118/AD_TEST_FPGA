// Copyright (C) 1991-2013 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

// PROGRAM		"Quartus II 64-Bit"
// VERSION		"Version 13.0.0 Build 156 04/24/2013 SJ Full Version"
// CREATED		"Mon Nov 11 20:10:45 2019"

module AD_TEST(
	inclk0,
	uart_rx,
	sdoa,
	sdob,
	cs,
	sclk,
	sdi,
	uart_tx
);


input wire	inclk0;
input wire	uart_rx;
input wire	sdoa;
input wire	sdob;
output wire	cs;
output wire	sclk;
output wire	sdi;
output wire	uart_tx;

wire	SYNTHESIZED_WIRE_55;
wire	SYNTHESIZED_WIRE_56;
wire	SYNTHESIZED_WIRE_3;
wire	[7:0] SYNTHESIZED_WIRE_4;
wire	SYNTHESIZED_WIRE_7;
wire	SYNTHESIZED_WIRE_57;
wire	[47:0] SYNTHESIZED_WIRE_9;
wire	SYNTHESIZED_WIRE_11;
wire	SYNTHESIZED_WIRE_58;
wire	SYNTHESIZED_WIRE_14;
wire	SYNTHESIZED_WIRE_15;
wire	[31:0] SYNTHESIZED_WIRE_16;
wire	SYNTHESIZED_WIRE_59;
wire	SYNTHESIZED_WIRE_22;
wire	[15:0] SYNTHESIZED_WIRE_23;
wire	[31:0] SYNTHESIZED_WIRE_28;
wire	SYNTHESIZED_WIRE_31;
wire	[7:0] SYNTHESIZED_WIRE_32;
wire	SYNTHESIZED_WIRE_35;
wire	SYNTHESIZED_WIRE_36;
wire	SYNTHESIZED_WIRE_37;
wire	[7:0] SYNTHESIZED_WIRE_38;
wire	[7:0] SYNTHESIZED_WIRE_39;
wire	[7:0] SYNTHESIZED_WIRE_40;
wire	SYNTHESIZED_WIRE_43;
wire	SYNTHESIZED_WIRE_45;
wire	SYNTHESIZED_WIRE_46;
wire	SYNTHESIZED_WIRE_47;
wire	SYNTHESIZED_WIRE_50;
wire	[15:0] SYNTHESIZED_WIRE_52;

assign	SYNTHESIZED_WIRE_50 = 0;



assign	SYNTHESIZED_WIRE_14 =  ~SYNTHESIZED_WIRE_55;


PLL	b2v_inst1(
	
	.inclk0(inclk0),
	.c0(SYNTHESIZED_WIRE_56)
	);


uart_tx	b2v_inst10(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.tx_vld(SYNTHESIZED_WIRE_3),
	.tx_data(SYNTHESIZED_WIRE_4),
	.uart_tx(uart_tx),
	.tx_rdy(SYNTHESIZED_WIRE_57));
	defparam	b2v_inst10.BAUD_RATE = 115200;
	defparam	b2v_inst10.CLK_M_ = 50;


conY2X	b2v_inst11(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.indat_vld(SYNTHESIZED_WIRE_7),
	.en(SYNTHESIZED_WIRE_57),
	.indat(SYNTHESIZED_WIRE_9),
	.outdat_vld(SYNTHESIZED_WIRE_35),
	.rdy(SYNTHESIZED_WIRE_47),
	.outdat(SYNTHESIZED_WIRE_38));
	defparam	b2v_inst11.X = 8;
	defparam	b2v_inst11.Y = 48;



rst	b2v_inst14(
	.clk(SYNTHESIZED_WIRE_56),
	.cmd_rst_n(SYNTHESIZED_WIRE_11),
	.rst_n(SYNTHESIZED_WIRE_55));


FIFO	b2v_inst15(
	.clock(SYNTHESIZED_WIRE_56),
	.rdreq(SYNTHESIZED_WIRE_58),
	.sclr(SYNTHESIZED_WIRE_14),
	.wrreq(SYNTHESIZED_WIRE_15),
	.data(SYNTHESIZED_WIRE_16),
	.empty(SYNTHESIZED_WIRE_45),
	.q(SYNTHESIZED_WIRE_28));


ad7380	b2v_inst2(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.sdoa(sdoa),
	.sdob(sdob),
	.cs(cs),
	.sclk(sclk),
	.sdi(sdi),
	.rxdata_vld(SYNTHESIZED_WIRE_59)
	);
	defparam	b2v_inst2.CMM1 = 16'b1001000000000000;
	defparam	b2v_inst2.CMM2 = 16'b1010000011111111;
	defparam	b2v_inst2.CMM3 = 16'b1011000000000000;
	defparam	b2v_inst2.CMM4 = 16'b1100000000000000;
	defparam	b2v_inst2.CMM5 = 16'b1101111111111111;
	defparam	b2v_inst2.CMR1 = 16'b0001000000000000;
	defparam	b2v_inst2.CMR2 = 16'b0010000000000000;
	defparam	b2v_inst2.CMR3 = 16'b0011000000000000;
	defparam	b2v_inst2.CMR4 = 16'b0100000000000000;
	defparam	b2v_inst2.CMR5 = 16'b0101000000000000;
	defparam	b2v_inst2.NOP = 16'b0000000000000000;


ad_set	b2v_inst3(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.dep_flag(SYNTHESIZED_WIRE_59),
	.intf_dep_en(SYNTHESIZED_WIRE_22),
	.intf_dep_data(SYNTHESIZED_WIRE_23),
	.ad_wr_en(SYNTHESIZED_WIRE_43),
	.msgH_vld(SYNTHESIZED_WIRE_7),
	
	.MesageEnder(SYNTHESIZED_WIRE_52),
	.MesageHeader(SYNTHESIZED_WIRE_9));
	defparam	b2v_inst3.CH = 1;
	defparam	b2v_inst3.DELAY_TIME = 1000;
	defparam	b2v_inst3.DEPTH = 256;
	defparam	b2v_inst3.FIRE_TIME_MS = 2000;
	defparam	b2v_inst3.SIZE = 4;


conY2X	b2v_inst4(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.indat_vld(SYNTHESIZED_WIRE_58),
	.en(SYNTHESIZED_WIRE_57),
	.indat(SYNTHESIZED_WIRE_28),
	.outdat_vld(SYNTHESIZED_WIRE_36),
	.rdy(SYNTHESIZED_WIRE_46),
	.outdat(SYNTHESIZED_WIRE_39));
	defparam	b2v_inst4.X = 8;
	defparam	b2v_inst4.Y = 32;


control	b2v_inst5(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.uart_in_vld(SYNTHESIZED_WIRE_31),
	.uart_in(SYNTHESIZED_WIRE_32),
	.intf_dep_en(SYNTHESIZED_WIRE_22),
	.cmd_rst_n(SYNTHESIZED_WIRE_11),
	.intf_dep_data(SYNTHESIZED_WIRE_23));
	defparam	b2v_inst5.OP_AD_DEPTH = 16'b0000000000000001;
	defparam	b2v_inst5.OP_RST = 16'b0000000000000000;


data_m2o	b2v_inst6(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.indat1_vld(SYNTHESIZED_WIRE_35),
	.indat2_vld(SYNTHESIZED_WIRE_36),
	.indat3_vld(SYNTHESIZED_WIRE_37),
	.indat1(SYNTHESIZED_WIRE_38),
	.indat2(SYNTHESIZED_WIRE_39),
	.indat3(SYNTHESIZED_WIRE_40),
	.outdat_vld(SYNTHESIZED_WIRE_3),
	.outdat(SYNTHESIZED_WIRE_4));
	defparam	b2v_inst6.DATA_W = 8;


fifo_inf	b2v_inst7(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.ad_wr_en(SYNTHESIZED_WIRE_43),
	.dout_vld(SYNTHESIZED_WIRE_59),
	.empty(SYNTHESIZED_WIRE_45),
	.tx_rdy(SYNTHESIZED_WIRE_46),
	.msg_end_en(SYNTHESIZED_WIRE_47),
	.fifo_wr(SYNTHESIZED_WIRE_15),
	.fifo_rd(SYNTHESIZED_WIRE_58),
	.temp_data(SYNTHESIZED_WIRE_16));
	defparam	b2v_inst7.T_DATA = 32'b01011010000011111010010110101010;
	defparam	b2v_inst7.TDATA_DW = 32;


conY2X	b2v_inst8(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.indat_vld(SYNTHESIZED_WIRE_50),
	.en(SYNTHESIZED_WIRE_57),
	.indat(SYNTHESIZED_WIRE_52),
	.outdat_vld(SYNTHESIZED_WIRE_37),
	
	.outdat(SYNTHESIZED_WIRE_40));
	defparam	b2v_inst8.X = 8;
	defparam	b2v_inst8.Y = 16;


uart_rx	b2v_inst9(
	.clk(SYNTHESIZED_WIRE_56),
	.rst_n(SYNTHESIZED_WIRE_55),
	.uart_rx(uart_rx),
	.rx_vld(SYNTHESIZED_WIRE_31),
	.rx_data(SYNTHESIZED_WIRE_32));
	defparam	b2v_inst9.BAUD_RATE = 115200;
	defparam	b2v_inst9.CLK_M_ = 50;


endmodule
