//���ڽ���ģ��
//�����ʣ�115200
//
module uart_rx(
    clk    ,
    rst_n  ,
    //�����ź�,����dout
    uart_rx     ,
    rx_data     ,
    rx_vld      
    );

    //��������
   parameter      CLK_M_ 			=         50;    //时钟频率：50M
	parameter      BAUD_RATE 		=         115200;    //波特率：115200
	reg[15:0]      baud_rate_cnt 	=         CLK_M_*1000000/BAUD_RATE;    //32_000_000/115200 = 278 
    //�����źŶ���
    input               clk    ;
    input               rst_n  ;
    input               uart_rx;

    //�����źŶ���
    //output[DATA_W-1:0]  dout   ;
    output              rx_data     ;
    output              rx_vld      ;
    //�����ź�reg����
    //reg   [DATA_W-1:0]  dout   ;
    reg[7:0]            rx_data     ;
    reg                 rx_vld      ;
    //�м��źŶ���
    //reg                 signal1;
    wire                add_cnt0,end_cnt0;
    wire                add_cnt1,end_cnt1;
    reg[15:0]           cnt0        ;
    reg[7:0]            cnt1        ;
    reg                 flag        ;
    reg                 uart_rx_ff0 ;
    reg                 uart_rx_ff1 ;
    reg                 uart_rx_ff2 ;
    wire                middle      ;

    always @(posedge clk or negedge rst_n)begin
        if(!rst_n)begin
           cnt0 <= 0;
       end
       else if(add_cnt0)begin
           if(end_cnt0)
              cnt0 <= 0;
         else
            cnt0 <= cnt0 + 1;
    end
end

assign add_cnt0 = flag;
assign end_cnt0 = add_cnt0 && cnt0== baud_rate_cnt-1;

always @(posedge clk or negedge rst_n)begin 
    if(!rst_n)begin
        cnt1 <= 0;
    end
    else if(add_cnt1)begin
        if(end_cnt1)
            cnt1 <= 0;
        else
            cnt1 <= cnt1 + 1;
    end
end

assign add_cnt1 = end_cnt0;
assign end_cnt1 = add_cnt1 && cnt1== 9-1;


//uart_rx_ff0
//uart_rx_ff1
//uart_rx_ff2
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        uart_rx_ff0 <= 1;
        uart_rx_ff1 <= 1;
        uart_rx_ff2 <= 1;
    end
    else begin
        uart_rx_ff0 <= uart_rx;
        uart_rx_ff1 <= uart_rx_ff0;
        uart_rx_ff2 <= uart_rx_ff1;
    end
end

//flag
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        flag <= 0;
    end
    else if(uart_rx_ff1==0 && uart_rx_ff2==1)begin  //uart_rx_ff1���½���
        flag <= 1;
    end
    else if(end_cnt1)begin
        flag <= 0;
    end
end

assign middle = add_cnt0&&cnt0==baud_rate_cnt/2-1;

//rx_data
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        rx_data <= 0;
    end
    else if(middle && cnt1>=1&&cnt1<9 ) begin
        rx_data[cnt1-1] <= uart_rx_ff2;
    end
end

//rx_vld
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        rx_vld <= 0;
    end
    else if(middle && cnt1==8)begin
        rx_vld <= 1;
    end
    else begin
        rx_vld <= 0;
    end
end

    endmodule

