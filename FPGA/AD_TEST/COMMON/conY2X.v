module conY2X(
    clk    ,
    rst_n  ,
    //�����ź�,����dout
    indat,
    indat_vld,
    en,
    outdat,
    outdat_vld,
    rdy
    );

    //��������
	 parameter      Y 		=         16;
	 parameter      X 		=         8;
	 
    //�����źŶ���
    input               clk    ;
    input               rst_n  ;
    input[Y-1:0]       indat;
    input               indat_vld;
    input               en;

    //�����źŶ���
    //output[DATA_W-1:0]  dout   ;
    output[X-1:0]       outdat;
    output              outdat_vld;
    output              rdy;
    //�����ź�reg����
    //reg   [DATA_W-1:0]  dout   ;
    reg [X-1:0]         outdat;
    reg                 outdat_vld;
    reg                 rdy = 1;
    
    //�м��źŶ���
    //reg                 signal1;
    reg [7:0]           cnt = 0;
    wire                add_cnt,end_cnt;
    reg [Y-1:0]        indat_ff0;
    reg                 is_conv;
 

    //indat_ff0
    always  @(posedge clk or negedge rst_n)begin    
        if(rst_n==1'b0)begin
            indat_ff0 <= 0;
        end
        else if(indat_vld)begin //��һ��
            indat_ff0 <= indat;
        end
    end

    //isconv
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            
            is_conv <= 0;
        end
        else if(indat_vld)begin //conv start
           
            is_conv <= 1;
        end
        else if(end_cnt)begin   //conv end
            is_conv <= 0;
        end
        
    end    
    
    //outdat
    always  @(*)begin       
        outdat = indat_ff0[(cnt+1)*X-1-:X];     //����ת��   
    end 

    //outdat_vld
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
            outdat_vld <= 0;
        end
        else if(is_conv && en)begin
            outdat_vld <= 1;
        end
        else begin
            outdat_vld <= 0;
        end
    end

     //cnt
    always @(posedge clk or negedge rst_n)begin
        if(!rst_n)begin
            cnt <= 0;
        end
        else if(add_cnt)begin
            if(end_cnt)
                cnt <= 0;
            else
                cnt <= cnt + 1;
        end
    end

    assign add_cnt = outdat_vld;    //�����������ݸ���      
    assign end_cnt = add_cnt && cnt== Y/X-1;

    
    //rdy
    always  @(*)begin
        if(is_conv||indat_vld)begin
            rdy = 0;
        end
        else begin
            rdy = 1;
        end
    end




   








    endmodule

