module rst(
    clk    ,
    cmd_rst_n  ,
    //�����ź�,����dout
    rst_n
    );

    //��������
    //parameter      DATA_W =         8;

    //�����źŶ���
    input               clk    ;
    input               cmd_rst_n  ;

    //�����źŶ���
    output  rst_n   ;

    //�����ź�reg����
    reg rst_n = 1   ;

    //�м��źŶ���
    reg                 flag = 1;
    reg[31:0]           cnt = 0;

    wire add_cnt,end_cnt;

    always @(posedge clk or negedge cmd_rst_n)begin
        if(!cmd_rst_n)begin
            cnt <= 0;
        end
        else if(add_cnt)begin
            if(end_cnt)
                cnt <= 0;
            else
                cnt <= cnt + 1;
        end
    end

    assign add_cnt = flag;       
    assign end_cnt = add_cnt && cnt== 5-1; 

    always  @(posedge clk or negedge cmd_rst_n)begin
        if(cmd_rst_n==1'b0)begin
            flag <= 1;
        end        
        else if(end_cnt)begin
            flag <= 0;
        end
    end

    always  @(posedge clk or negedge cmd_rst_n)begin
        if(cmd_rst_n==1'b0)begin
            rst_n <= 0;
        end
        else if(end_cnt)begin
            rst_n <= 1;
        end
        
    end







    endmodule

