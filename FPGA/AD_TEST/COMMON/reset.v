module reset(
						clk,
						rst_n
						);
			
input clk;
output reg rst_n=1'b0;

reg[26:0] cnt=24'd0;

wire clr,pulse;
assign clr=~rst_n;
assign pulse=(cnt==27'd96_000_000);   //should be 24'd16000000

always@(negedge clk or negedge clr)
	if(!clr) 
			cnt<=27'd0;
	else
			cnt<=cnt+1'b1;

always@(posedge pulse)
	rst_n<=1'b1;
endmodule
