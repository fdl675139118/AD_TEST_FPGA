//
//ʱ��32M
//�����ʣ�115200
//
module uart_tx(
    clk    ,
    rst_n  ,
    //�����ź�,����dout
    tx_vld      ,
    tx_data     ,
    uart_tx     ,
    tx_rdy      
    );

    //��������
    parameter      CLK_M_ 			=         50;    //时钟频率：50M
	 parameter      BAUD_RATE 		=         115200;    //波特率：115200
	 reg[15:0]      baud_rate_cnt 	=         CLK_M_*1000000/BAUD_RATE;    //32_000_000/115200 = 278 
	 //reg[15:0]      baud_rate_cnt 	=         434;     
    //�����źŶ���
    input               clk    ;
    input               rst_n  ;
    input               tx_vld ;
    input[7:0]          tx_data;

    //�����źŶ���
    //output[DATA_W-1:0]  dout   ;
    output              uart_tx;
    output              tx_rdy ;

    //�����ź�reg����
    //reg   [DATA_W-1:0]  dout   ;
    reg                 uart_tx;
    reg                 tx_rdy = 1 ;

    //�м��źŶ���
    //reg                 signal1;
    wire add_cnt0,      end_cnt0;
    wire add_cnt1,      end_cnt1;

    reg[15:0]           cnt0    ;
    reg[7:0]            cnt1    ;

    reg                 flag    ;
    reg[9:0]            tx_data_tmp;

always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        cnt0 <= 0;
    end
    else if(add_cnt0)begin
        if(end_cnt0)
            cnt0 <= 0;
        else
            cnt0 <= cnt0 + 1;
    end
end

assign add_cnt0 = flag;
assign end_cnt0 = add_cnt0 && cnt0== baud_rate_cnt-1;

always @(posedge clk or negedge rst_n)begin 
    if(!rst_n)begin
        cnt1 <= 0;
    end
    else if(add_cnt1)begin
        if(end_cnt1)
            cnt1 <= 0;
        else
            cnt1 <= cnt1 + 1;
    end
end

assign add_cnt1 = end_cnt0;
assign end_cnt1 = add_cnt1 && cnt1== 10-1;

//flag
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        flag <= 0;
    end
    else if(tx_vld)begin
        flag <= 1;
    end
    else if(end_cnt1)begin
        flag <= 0;
    end
end

//tx_data_tmp
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        tx_data_tmp <= 0;
    end
    else if(tx_vld) begin
        tx_data_tmp <= {1'b1,tx_data,1'b0};
    end
end

//uart_tx
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        uart_tx <= 1;
    end
    else if(add_cnt0 && cnt0==1-1) begin
        uart_tx <= tx_data_tmp[cnt1];
    end
end

//tx_rdy
always  @(*)begin
    if(flag || tx_vld)
        tx_rdy <= 0;
    else
        tx_rdy <= 1;

end


endmodule





