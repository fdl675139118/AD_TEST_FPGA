module fifo_inf(
    clk    ,
    rst_n  ,
    //锟斤拷锟斤拷锟脚猴拷,锟斤拷锟斤拷dout
    ad_wr_en        ,
    dout_vld        ,
    empty           ,
    tx_rdy          ,
    fifo_wr         ,
    fifo_rd			  ,
	 temp_data		  ,
	 msg_end_en
    );

    //锟斤拷锟斤拷锟斤拷锟斤拷
    parameter      TDATA_DW =         32;
	 parameter      T_DATA   =         32'h5A_0F_A5_AA;
    //锟斤拷锟斤拷锟脚号讹拷锟斤拷
    input               clk    ;
    input               rst_n  ;
    input               ad_wr_en;
    input               dout_vld;
    input               empty;
    input               tx_rdy;
	 input					msg_end_en;
    //锟斤拷锟斤拷锟脚号讹拷锟斤拷
    //output[DATA_W-1:0]  dout   ;
    output              fifo_wr;
    output              fifo_rd;
	 output					temp_data;

    //锟斤拷锟斤拷锟脚猴拷reg锟斤拷锟斤拷
    //reg   [DATA_W-1:0]  dout   ;
    reg                 fifo_wr;
    reg                 fifo_rd;
	 reg[TDATA_DW-1:0]				temp_data = T_DATA;
    //锟叫硷拷锟脚号讹拷锟斤拷
    //reg                 signal1;

    

    //fifo_wr
    always  @(*)begin
        if(ad_wr_en)begin            
            fifo_wr = dout_vld;
        end
        else begin
            fifo_wr = 0;            
        end
    end

    //fifo_rd
    always  @(posedge clk or negedge rst_n)begin
        if(rst_n==1'b0)begin
             fifo_rd <= 0;
        end
        else if(!empty && msg_end_en)begin
             fifo_rd <= tx_rdy;
        end
        else begin
             fifo_rd <= 0;
        end
    end


    endmodule

