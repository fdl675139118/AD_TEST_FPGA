module control(
    clk    ,
    rst_n  ,
    //�����ź�,����dout
    uart_in         ,
    uart_in_vld     ,
    intf_dep_en     ,
    intf_dep_data   ,
	 cmd_rst_n
    );

    //��������
   
    //�����źŶ���
    input               clk         ;
    input               rst_n       ;
    input[7:0]          uart_in     ;
    input               uart_in_vld ;

    //�����źŶ���
    output[15:0]  intf_dep_data     ;
    output        intf_dep_en       ;
	 output			cmd_rst_n			;

    //�����ź�reg����
    //reg   [DATA_W-1:0]  dout   ;

    //�м��źŶ���
    //reg                 signal1;

    reg [15:0] opcode_cnt;
    wire add_opcode_cnt,end_opcode_cnt;

    always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        opcode_cnt <= 0;
    end
    else if(add_opcode_cnt)begin
        if(end_opcode_cnt)
            opcode_cnt <= 0;
        else
            opcode_cnt <= opcode_cnt + 1;
    end
end

assign add_opcode_cnt = uart_in_vld;   //uart_in_vld       
assign end_opcode_cnt = add_opcode_cnt && opcode_cnt== 4-1;  //opcode len :4   

reg [15:0] opcode_instr;
//opcode_instr
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        opcode_instr <= 0;
    end
    else if(add_opcode_cnt && opcode_cnt<=2-1)begin
        opcode_instr[(opcode_cnt+1)*8-1-:8] <= uart_in;
    end
end

reg [15:0] opcode_data;
//opcode_data
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        opcode_data <= 0;
    end
    else if(add_opcode_cnt && opcode_cnt>2-1) begin
        opcode_data[(opcode_cnt+1-2)*8-1-:8] <= uart_in;
    end
end

reg opcode_vld;
//opcode_vld
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        opcode_vld <= 0;
    end
    else if(end_opcode_cnt)begin
        opcode_vld <= 1;
    end
    else begin
        opcode_vld <= 0;
    end
end


parameter OP_AD_DEPTH = 16'h0001;

reg intf_dep_en;
//intf_dep_en
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        intf_dep_en <= 0;
    end
    else if(opcode_vld && opcode_instr == OP_AD_DEPTH)begin
        intf_dep_en <= 1;
    end
    else begin
        intf_dep_en <= 0;
    end
end

reg[15:0] intf_dep_data;
//intf_dep_data
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        intf_dep_data <= 0;
    end
    else if(opcode_vld && opcode_instr == OP_AD_DEPTH) begin
        intf_dep_data <= opcode_data;
    end
end

parameter OP_RST = 16'h0000;

reg cmd_rst_n=1;
//cmd_rst_n
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        cmd_rst_n <= 1;
    end
    else if(opcode_vld && opcode_instr == OP_RST)begin
        cmd_rst_n <= 0;
    end
    else begin
        cmd_rst_n <= 1;
    end
end


    endmodule














